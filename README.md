## Running


### [1. 1]Setting the Node Application

Make sure MongoDB is Listening on port 27017.  You can check that by using `mongo` on shell.  The output will show a mongo shell.  If eveyrthing go right , open new terminal .
### [1.2]   Running the Node Application

Use `npm start` for running the app. Once you see Express is running on port `3000` go ahead to REST Client/Browser and give a request to  `http://localhost:3000/scrap_the_web`.
Request Command :- 
`curl http://localhost:3000/scrap_the_web` or
 `wget  http://localhost:3000/scrap_the_web`
Default is medium.com. 
You can pass parameters for other websites too. 
`curl http://localhost:3000/scrap_the_web/?url="www.example.com" `

### [1.3]   Testing the Node Application

`npm test` will run the tests defined for express application .Make sure the Express is Up and Listening. Use new Terminal for testing the app. 

### [2. 1] Setting the Node Application with Docker 
Make sure you have docker up and running on your host.
Use `npm production` and the express server will lift up at port 3000.  you can then use **[1.2]** to test the same.

### [3. 1]  Accessing Database. 


 Open mongo shell locally and type `show dbs`.
 You will see DB named `Rentomojo_Prod`.
  Use `show collections`. 
  You will use collection named `urls`.
  See all queries at collection with `db.urls.find()`.
  
## Screenshots


![enter image description here](https://raw.githubusercontent.com/Ayushverma8/RentoMojo-Task/master/Pictures/ScreenshotLogic.png)
## TODO
1. Improve concurrent_request.js
2. E2E Tests for all code with 100% Coverage. 